$('#menu').show();

if (Modernizr.getusermedia) {
    var getUM = Modernizr.prefixed('getUserMedia', navigator);
    getUM({
            audio: false,
            video: true
        },
        function (stream) {
            var video = document.getElementById('vid');
            video.srcObject = stream;
            video.onloadedmetadata = function (e) {
                video.play();
            };

            if ($('#vid').width() > $('#vid').height()) {
                $('#vid').css('height', '100%');
                $('#vid').css('margin-left', '-' + (($('#vid').width() - $(window).width()) / 2));
            } else {
                $('#vid').css('width', '100%');
            }
        },
        function (err) {
            console.log('The following error occurred: ' + err.name);
        });
} else {
    console.log('getUserMedia not supported');
}

function goto(targetFrame) {
    $('.frame').hide();
    $('#' + targetFrame).show();
}

var buildStage = 1;

function done() {
    buildStage++;
    build(buildStage);
}

function build(stage) {
    if (stage == 1) {
        $('#grab').css('opacity', 0);
        $('#ready').css('opacity', 0);
        flashIndicate('Hey! Let\'s get started.', 1000, function () {
            $('#ready').animate({
                opacity: 1
            }, 2000);

            flashIndicate('Stand in a corner of the room.', 5000, function () {

            });
        });
    } else if (stage == 2) {
        $('#ready').animate({
            opacity: 0
        }, 500, function () {
            $('#ready').animate({
                opacity: 1
            }, 2000);
        });

        flashIndicate('Awesome! Now aim the crosshair at a corner of the floor.', 10000, function () {

        });
    } else if (stage == 3) {
        $('#ready').animate({
            opacity: 0
        }, 500);

        $('#grab').animate({
            opacity: 1
        }, 1000);

        flashIndicate('Hit \'grab\' as you aim at every corner of the floor.', 4000, function () {
            $('#ready').text('Begin');
            $('#ready').animate({
                opacity: 1
            }, 2000);
            flashIndicate('We\'ll save your work as you go. Get to work!', 5000, function () {});
        });
    } else if (stage == 4) {
        $('#ready').animate({
            opacity: 0
        }, 500);
    }
}

function grab() {

}

var flashClearId = 0;

function flashIndicate(text, hold, cb) {
    clearTimeout(flashClearId);
    $('#build-indicator').hide();
    $('#build-indicator').html(text);
    $('#build-indicator').fadeIn(1000, function () {
        flashClearId = setTimeout(function () {
            $('#build-indicator').fadeOut(1000, function () {
                cb();
            });
        }, hold);
    });
}

var gn = new GyroNorm();

gn.init().then(function () {
    gn.start(function (data) {
        console.log(data.do.beta);
        // data.do.alpha	( deviceorientation event alpha value )
        // data.do.beta		( deviceorientation event beta value )
        // data.do.gamma	( deviceorientation event gamma value )
        // data.do.absolute	( deviceorientation event absolute value )
    });
}).catch(function (e) {
    // Catch if the DeviceOrientation or DeviceMotion is not supported by the browser or device
});