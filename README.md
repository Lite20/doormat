# Doormat #

Doormat is an opensource service & tool for mapping the interiors of buildings! This repository is the home of the Doormat web-client.

### Do I need my own server? ###

We try to run a public Doormat server for all to share and test, which should also act as a central repository, however it can't handle everyone. The limit is reasonable but we urge you to create a server of your own! For 5$ you can get one from digital ocean or competitors.

### Embedding Doormat ###

Embedding Doormat in your client is a fantastic idea if your service needs it, and has a lot of clients that will contribute. Simply embed the HTML client in your app. The server REST API is also documented should you desire to make a native client.

### Contributing ###

There are lots of ways to contribute to the Doormat client! (to help with the server, hop on over to the server repository). We love contributions, but please create a thread first to propose your idea. We don't want to waste your time tweaking everything to integrate better. Our favorite contributions are those that:

* Stability of the Doormat Client
* Speed of the Doormat Client
* UI of the Doormat Client
* Security of the Doormat Client

Make sure to make a thread and chuck your ideas at us!

### Have a question? ###

Go ahead and make a thread! We're more than glad to answer questions as there is no documentation at the present moment.